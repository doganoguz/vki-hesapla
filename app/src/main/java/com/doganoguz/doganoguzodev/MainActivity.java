package com.doganoguz.doganoguzodev;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {


    private RadioButton erkek;
    private RadioButton kadin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        final EditText boy=(EditText)findViewById(R.id.boy);
        final EditText kilo=(EditText)findViewById(R.id.kilo);

        Button hesapla=(Button)findViewById(R.id.hesapla);
        erkek = (RadioButton)findViewById(R.id.erkek);
        kadin = (RadioButton)findViewById(R.id.kadin);

        hesapla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                float hFloatValue=Float.parseFloat(boy.getText().toString());
                float wFloatValue=Float.parseFloat(kilo.getText().toString());
                float sonuc = wFloatValue/((hFloatValue / 100) * (hFloatValue / 100));
                float idealWeight = 0;

                RadioGroup g = (RadioGroup) findViewById(R.id.radiogr);
                switch (g.getCheckedRadioButtonId())
                {
                    case R.id.kadin:
                   idealWeight = (float) ((hFloatValue-100)*0.89);
                        break;

                    case R.id.erkek:
                        idealWeight = (float) ((hFloatValue-100)*0.94);
                        break;
                }



                Intent myIntent = new Intent(MainActivity.this, sonuc.class);
                myIntent.putExtra("myIdealWeight", Float.toString(idealWeight));
                myIntent.putExtra("myCurrentAverage", Float.toString(sonuc));
                startActivity(myIntent);



            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cikis) {
            System.exit(0);
            return true;
        }
        else if (id == R.id.hakkimizda) {
            startActivity(new Intent(MainActivity.this, hakkimizda.class));
            finish();
        }


        return super.onOptionsItemSelected(item);
    }
}
