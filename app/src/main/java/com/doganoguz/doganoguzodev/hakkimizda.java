package com.doganoguz.doganoguzodev;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class hakkimizda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hakkimizda);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Egolu değilim sadece buton id bunu koymak daha kolay geldi dsfjhsdfkjh
        final Button ben = (Button) findViewById(R.id.ben);

        //Buton click olayı
        ben.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Yönlendireceğimiz siteyi yazıyoruz
                Uri uri = Uri.parse("http://doganoguz.com/");

                //Uri buradan bağlantı adresini alıp telefonun içerisinde ki ana tarayıcıya yönleniyor
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);

                //Ve intent başlatıp tarayıcıda siteyi açıyor
                startActivity(intent);

            }
        });

    }

    //Telefonun geri tuşuna basıldığın an ki olay
    @Override
    public void onBackPressed() {
        startActivity(new Intent(hakkimizda.this, MainActivity.class));
        finish();
    }


}
