package com.doganoguz.doganoguzodev;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class sonuc extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sonuc);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView sonuc;
        TextView aciklama;



        float idealWeight = 0;
        float myAverage = 0;

        sonuc = (TextView) findViewById(R.id.sonuc1);
        aciklama = (TextView) findViewById(R.id.sonuc2);

        idealWeight = Float.parseFloat(getIntent().getStringExtra("myIdealWeight"));
        myAverage = Float.parseFloat(getIntent().getStringExtra("myCurrentAverage"));

        if(idealWeight > 0){
            sonuc.setText(getString(R.string.kilo)+": "+Math.round(idealWeight));
        }else{
            sonuc.setText(getString(R.string.val001));
        }

        if(Math.round(myAverage) >0 && Math.round(myAverage)<18.49){
            aciklama.setText(getString(R.string.val002));
        }else if(Math.round(myAverage) >18.49 && Math.round(myAverage)<24.99){
            aciklama.setText(getString(R.string.val003));
        }else if(Math.round(myAverage) >24.99 && Math.round(myAverage)<29.99){
            aciklama.setText(getString(R.string.val004));
        }else if(Math.round(myAverage) >29.99 && Math.round(myAverage)<34.99){
            aciklama.setText(getString(R.string.val005));
        }else if(Math.round(myAverage) >34.99 && Math.round(myAverage)<44.99){
            aciklama.setText(getString(R.string.val006));
        }else if(Math.round(myAverage) >44.99){
            aciklama.setText(getString(R.string.val007));
        }



    }
    //Telefonun geri tu?una bas?ld???n an ki olay
    @Override
    public void onBackPressed() {
        startActivity(new Intent(sonuc.this, MainActivity.class));
        finish();
    }
}
